﻿<%@ Page Title="" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="viewFeedback.aspx.cs" Inherits="admin_pages_viewFeedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentBody" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
 <ContentTemplate>
 <h3 style="display:inline">Welcome:</h3>
 <h3>Users feedback:</h3>
     <asp:LoginStatus ID="LoginStatus1" runat="server" CssClass="tip" 
         LogoutPageUrl="~/Default.aspx"/>
     &nbsp;
     <br />
     <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
         AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" 
         GridLines="None" PageSize="5" EnableModelValidation="True" 
         AllowSorting="True">
         <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
         <Columns>
             <asp:BoundField DataField="Column1" HeaderText="Question Count" 
                 SortExpression="Column1" ReadOnly="True" />
             <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
         </Columns>
         <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
         <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
         <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
         <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
         <AlternatingRowStyle BackColor="White" />
     </asp:GridView>
     <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
         ConnectionString="<%$ ConnectionStrings:trafficSignConnectionString1 %>" 
         
         
         SelectCommand="SELECT COUNT([WQID]), Name FROM [result] WHERE ([pass] = @pass) GROUP BY Name">
         <SelectParameters>
             <asp:Parameter DefaultValue="true" Name="pass" Type="Boolean" />
         </SelectParameters>
     </asp:SqlDataSource>
     <br />
     
    </ContentTemplate>
       </asp:UpdatePanel>
</asp:Content>

