﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class feedback : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TextArea1.Focus();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = WebConfigurationManager.ConnectionStrings["trafficSignConnectionString1"].ConnectionString;
            SqlCommand comm = new SqlCommand();
            comm.Connection = conn;
            comm.CommandText = "insert into Feedback(Text,Date) values(@feed,'" + DateTime.Now.Date.ToShortDateString().ToString() + " " + DateTime.Now.ToShortTimeString().ToString() + "');";
            comm.CommandType = System.Data.CommandType.Text;
            comm.Parameters.AddWithValue("@feed", TextArea1.Value);
            conn.Open();
            comm.ExecuteNonQuery();
            conn.Close();
        }
        Page.Controls.Remove(Panel1);
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "<script type='text/javascript' language='javascript'>alert('Thank You !'); window.location = 'Default.aspx';</script>");
    }

}
