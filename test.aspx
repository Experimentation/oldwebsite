﻿<%@ Page Title="" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="test.aspx.cs" Inherits="test" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentBody" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div style="margin-left:5px;">
    <asp:Label ID="Label1" runat="server">Welcome: </asp:Label><br />
    <asp:Label ID="Label4"
        runat="server" Text=""></asp:Label><br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="Label2" runat="server" Text="Label">Question: </asp:Label><br />
            <asp:Label ID="Label3" runat="server"></asp:Label><br />
            <div class="image">
            <asp:Image ID="Image1" runat="server"/>
            </div>
            <div class="radioB">
            <asp:RadioButtonList ID="RadioButtonList1" runat="server">
            <asp:ListItem></asp:ListItem>
            <asp:ListItem></asp:ListItem>
            <asp:ListItem></asp:ListItem>
            <asp:ListItem></asp:ListItem>
            </asp:RadioButtonList>
            </div>
            <br />
                    </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Button1" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="Button2" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
        </Triggers>
    </asp:UpdatePanel>
            <asp:Button ID="Button1" runat="server" Text="Next Question" 
                onclick="Button1_Click" CssClass="btn"/><asp:Button ID="Button2" 
                runat="server" Text="End" onclick="Button2_Click" />&nbsp; <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                    runat="server" ErrorMessage="Please choose an answer" 
                ControlToValidate="RadioButtonList1"></asp:RequiredFieldValidator>
            <asp:Timer ID="Timer1" runat="server" Enabled="False" Interval="1000" 
                ontick="Timer1_Tick1">
            </asp:Timer>
</div>
</asp:Content>

