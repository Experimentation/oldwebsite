﻿          var l = null;
          var map = null;
          function GetMap() {
              map = new VEMap('myMap');
              map.AttachEvent("oncredentialserror", MyCredentialsError);
              map.AttachEvent("oncredentialsvalid", MyCredentialsValid);
              map.SetCredentials("[Credentials]");
              map.LoadMap();
              map.HideDashboard();
              map.SetMapStyle(VEMapStyle.Shaded);
              map.AttachEvent('onchangeview', zoom);
          }

          function MyCredentialsError() {
              alert("The credentials are invalid.");
          }

          function MyCredentialsValid() {
             

          }
          function FindLoc(LOC, end) {
              try {
                  l = LOC;
                  var temp = LOC.toString().split("(", 1);
                  map.Find(null, temp + ", United States");
                  if (end) {
                      PIN(map, LOC);
                      map.DetachEvent('onchangeview', zoom);
                  }
              }
              catch (e) {
                  alert(e.message);
              }
          }
          function zoom() {
              PIN(map,l);
              //map.SetZoomLevel(4);
          }
          function PIN(theMap, name) {
                  var center = theMap.GetCenter();
                  var pin = theMap.AddPushpin(center);
                  pin.SetTitle(name);
          }