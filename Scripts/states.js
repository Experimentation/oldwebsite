﻿var i = 0;
var t;
var m = null;
var mystates = new Array("Alabama (Admission to union:December 14, 1819 ; Capital:Montgomery ; Most populous city: Birmingham)",
 "Alaska(Admission to union:January 3, 1959 ; Capital: Juneau ; Most populous city: Anchorage)",
 "Arizona(Admission to union:February 14, 1912 ; Capital: Phoenix; Most populous city: Phoenix)",
 "Arkansas(Admission to union:June 15, 1836 ; Capital: Little Rock; Most populous city: Little Rock)",
 "California(Admission to union:September 9, 1850 ; Capital: Sacramento; Most populous city: Los Angeles)",
 "Colorado(Admission to union:August 1, 1876 ; Capital: Denver; Most populous city: Denver)",
 "Connecticut(Admission to union:January 9, 1788 ; Capital: Hartford; Most populous city: Bridgeport)",
 "Delaware(Admission to union:December 7, 1787 ; Capital: Dover; Most populous city: Wilmington)",
 "Florida(Admission to union:March 3, 1845 ; Capital: Tallahassee; Most populous city: Jacksonville)",
 "Georgia(Admission to union:January 2, 1788 ; Capital: Atlanta; Most populous city: Atlanta)",
 "Hawaii(Admission to union:August 21, 1959 ; Capital: Honolulu; Most populous city: Honolulu)",
 "Idaho(Admission to union:July 3, 1890 ; Capital: Boise; Most populous city: Boise)",
 "Illinois(Admission to union:December 3, 1818 ; Capital: Springfield; Most populous city: Chicago)",
 "Indiana(Admission to union:December 11, 1816 ; Capital: Indianapolis; Most populous city: Indianapolis)",
 "Iowa(Admission to union:December 28, 1846 ; Capital: Des Moines; Most populous city: Des Moines)",
 "Kansas(Admission to union:January 29, 1861 ; Capital: Topeka; Most populous city: Wichita)",
 "Kentucky(Admission to union:June 1, 1792 ; Capital: Frankfort; Most populous city: Louisville)",
 "Louisiana(Admission to union:April 30, 1812 ; Capital: Baton Rouge; Most populous city: New Orleans)",
 "Maine(Admission to union:March 15, 1820 ; Capital: Augusta; Most populous city: Portland)",
 "Maryland(Admission to union:April 28, 1788 ; Capital: Annapolis; Most populous city: Baltimore)",
 "Massachusetts(Admission to union:February 6, 1788 ; Capital: Boston; Most populous city: Boston)",
 "Michigan(Admission to union:January 26, 1837 ; Capital: Lansing; Most populous city: Detroit)",
 "Minnesota(Admission to union:May 11, 1858 ; Capital: Saint Paul; Most populous city: Minneapolis)",
 "Mississippi(Admission to union:December 10, 1817 ; Capital: Jackson; Most populous city: Jackson)",
 "Missouri(Admission to union:August 10, 1821 ; Capital: Jefferson City; Most populous city: Kansas City)",
 "Montana(Admission to union:November 8, 1889 ; Capital: Helena; Most populous city: Billings)",
 "Nebraska(Admission to union:March 1, 1867 ; Capital: Lincoln; Most populous city: Omaha)",
 "Nevada(Admission to union:October 31, 1864 ; Capital: Carson City; Most populous city: Las Vegas)",
 "New Hampshire(Admission to union:June 21, 1788 ; Capital: Concord; Most populous city: Manchester)",
 "New Jersey(Admission to union:December 18, 1787 ; Capital: Trenton; Most populous city: Newark)",
 "New Mexico(Admission to union:January 6, 1912 ; Capital: Santa Fe; Most populous city: Albuquerque)",
 "New York(Admission to union:July 26, 1788 ; Capital: Albany; Most populous city: New York)",
 "North Carolina(Admission to union:November 21, 1789 ; Capital: Raleigh; Most populous city: Charlotte)",
 "North Dakota(Admission to union:November 2, 1889 ; Capital: Bismarck; Most populous city: Fargo)",
 "Ohio(Admission to union:March 1, 1803 ; Capital: Columbus; Most populous city: Columbus)",
 "Oklahoma(Admission to union:November 16, 1907 ; Capital: Oklahoma City; Most populous city: Oklahoma City)",
 "Oregon(Admission to union:February 14, 1859 ; Capital: Salem; Most populous city: Portland)",
 "Pennsylvania(Admission to union:December 12, 1787 ; Capital: Harrisburg; Most populous city: Philadelphia)",
 "Rhode Island(Admission to union:May 29, 1790 ; Capital: Providence; Most populous city: Providence)",
 "South Carolina(Admission to union:May 23, 1788 ; Capital: Columbia; Most populous city: Columbia)",
 "South Dakota(Admission to union:November 2, 1889 ; Capital: Pierre; Most populous city: Sioux Falls)",
 "Tennessee(Admission to union:June 1, 1796 ; Capital: Nashville; Most populous city: Memphis)",
 "Texas(Admission to union:December 29, 1845 ; Capital: Austin; Most populous city: Houston)",
 "Utah(Admission to union:January 4, 1896 ; Capital: Salt Lake City; Most populous city: Salt Lake City)",
 "Vermont(Admission to union:March 4, 1791 ; Capital: Montpelier; Most populous city: Burlington)",
 "Virginia(Admission to union:June 25, 1788 ; Capital: Richmond; Most populous city: Virginia Beach)",
 "Washington(Admission to union:November 11, 1889 ; Capital: Olympia; Most populous city: Seattle)",
 "West Virginia(Admission to union:June 20, 1863 ; Capital: Charleston; Most populous city: Charleston)",
 "Wisconsin(Admission to union:May 29, 1848 ; Capital: Madison; Most populous city: Milwaukee)",
 "Wyoming(Admission to union:July 10, 1890 ; Capital: Madison; Most populous city: Cheyenne)");


function start() {
    GetMap();
    t = setInterval("DO()", 4000);
}
function DO(){
    if (i < 49) {
        if (i == 48) { FindLoc(mystates[i], true); }
        else { FindLoc(mystates[i], false); }
        ++i;
    }
    else {
        clearInterval(t);
        alert("END");
    }
}