﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for qRand
/// </summary>
public class qRand
{
    List<int> fiftyQ = new List<int>();
	public qRand()
	{
        
	}
    public List<int> FiftyQ() {
        do
        {
            int temp = new Random().Next(1, 51);
            if (!fiftyQ.Contains(temp))
            {
                fiftyQ.Add(temp);
            }
        }
        while (fiftyQ.Count < 6);
            return fiftyQ;
    }
}
