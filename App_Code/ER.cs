﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

/// <summary>
/// Summary description for ER
/// </summary>
public class ER
{
	public ER()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static void recordeError(Exception err , string url){
        try
        {

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load("~XMLError.xml");
            System.Xml.XmlElement newE = doc.CreateElement("Error");
            System.Xml.XmlAttribute newAtt = doc.CreateAttribute("date");

            newAtt.Value = DateTime.Now.Date.ToShortDateString().ToString();
            newE.Attributes.Append(newAtt);
            newE.InnerXml = "<url>" + url + "</url><message>" + err.Message.ToString() + "</message><stack>" + err.StackTrace.ToString() + "</stack>";
            doc.DocumentElement.AppendChild(newE);
            System.Xml.XmlTextWriter wrtr = new System.Xml.XmlTextWriter("XMLError.xml", Encoding.UTF8);
            doc.WriteTo(wrtr);
            wrtr.Close();
        }
        catch (Exception e2)
        {

        }
    }
}
