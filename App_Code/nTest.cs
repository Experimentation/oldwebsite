﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Data.SqlClient;

/// <summary>
/// Summary description for nTest
/// </summary>
public class nTest
{
     string connString = WebConfigurationManager.ConnectionStrings["trafficSignConnectionString1"].ConnectionString;
     string a1, a2, a3, a4, img;
     int pid, R;
     List<string> temp = new List<string>();

	public nTest()
	{
        
	}

    public  List <string> ask(int qNo){
        using (SqlConnection sqlConn = new SqlConnection(connString))
            {
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = sqlConn;
                sqlComm.CommandType = System.Data.CommandType.Text;
                sqlComm.CommandText = "select * from pAnswers where (ID = @id);";
                sqlComm.Parameters.AddWithValue("@id", qNo);
                sqlConn.Open();
                /*
                 *       cmd.Parameters.Add("@user",
               SqlDbType.NVarChar, 10).Value = username;
                 * */
                SqlDataReader sRead = sqlComm.ExecuteReader();
                while (sRead.Read())
                {
                    int.TryParse(sRead["picID"].ToString(),out pid);
                    int.TryParse(sRead["R"].ToString(),out R);
                    a1 = sRead["a1"].ToString();
                    a2 = sRead["a2"].ToString();
                    a3 = sRead["a3"].ToString();
                    a4 = sRead["a4"].ToString();
                }
                sRead.Close();
                sqlComm.CommandText = "select path from pictures where (picID = @pid)";
                sqlComm.Parameters.AddWithValue("@pid", pid);
                img = sqlComm.ExecuteScalar().ToString();
                sqlConn.Close();
            }

        temp.Add("~/image/" + img + ".PNG");
        temp.Add(a1);
        temp.Add(a2);
        temp.Add(a3);
        temp.Add(a4);
        return temp;
    }

    public  int rResult()
    {
        return R;
    }

    public void processResult(int id, string name, bool pass)
    {
        //datetime
        using (SqlConnection sqlConn2 = new SqlConnection(connString))
        {
            SqlCommand sqlComm2 = new SqlCommand();
            sqlComm2.Connection = sqlConn2;
            sqlComm2.CommandType = System.Data.CommandType.Text;
            if (pass)
            {
                sqlComm2.CommandText = "insert into result(WQID, Name, pass, date) values(" + id + ",@N," + 1 + ",'" + DateTime.Now.Date.ToShortDateString().ToString() + "');";
            }
            else
            {
                sqlComm2.CommandText = "insert into result(WQID, Name, pass, date) values(" + id + ",@N," + 0 + ",'" + DateTime.Now.Date.ToShortDateString().ToString() + "');";

            }
            sqlComm2.Parameters.AddWithValue("@N", name);
            sqlConn2.Open();
            sqlComm2.ExecuteNonQuery();
            sqlConn2.Close();
        }
    }
}
