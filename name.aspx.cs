﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class name : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        nn.Focus();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Session["nName"] = nn.Text;
        if (Request.QueryString["learn"].ToString().Equals("false"))
        {
            Response.Redirect("test.aspx?learn=false");
        }
        else
        {
            Response.Redirect("test.aspx?learn=true");
        }
    }
}
