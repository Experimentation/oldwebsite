﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class map : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterClientScriptInclude("selective", ResolveUrl(@"~/Scripts/map.js"));
        Page.ClientScript.RegisterClientScriptInclude("selective2", ResolveUrl(@"~/Scripts/states.js"));
        if (!Page.ClientScript.IsStartupScriptRegistered("at"))
        {
            Page.ClientScript.RegisterStartupScript
                            (this.GetType(), "at", "start();", true);
        }
    }
}
