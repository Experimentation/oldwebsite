﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ImageTip.tipTableTableAdapters;

namespace ImageTip
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://www.justfortest.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {

        [WebMethod]
    public string getTip(int qNo)
        {
            string Tip = string.Empty;
            if (qNo > 0 || qNo < 51)
            {
                picturesTableAdapter pta = new picturesTableAdapter();
                tipTable.picturesDataTable pdt = pta.GetData(qNo);
                tipTable.picturesRow row = (tipTable.picturesRow)pdt.Rows[0];
                Tip = row.tip;
                return Tip;
            }
            else
            {
                Tip = "Question No out of range";
                return Tip;
            }
        }
    }
}
