﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Result : System.Web.UI.Page
{
    string connString = WebConfigurationManager.ConnectionStrings["trafficSignConnectionString1"].ConnectionString;
    int qright = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
            if (!Page.IsPostBack)
            {
                using (SqlConnection sqlConn = new SqlConnection(connString))
                {
                    SqlCommand sqlComm = new SqlCommand();
                    sqlComm.Connection = sqlConn;
                    sqlComm.CommandType = System.Data.CommandType.Text;
                    sqlComm.CommandText = "select count(pass) from result where (Name = @n and pass = 1);";
                    sqlComm.Parameters.AddWithValue("@n", Session["nName"].ToString());
                    sqlConn.Open();
                    qright = Convert.ToInt32(sqlComm.ExecuteScalar());
                    sqlConn.Close();
                }
                int temp = Convert.ToInt32(Session["qCount"]);
                if (qright == temp)
                {
                    Label1.Text = "Congradulations !";
                    Label1.Attributes.Add("style","display:block;margin-top:20px;margin-bottom:20px;border: thick ridge #00FF00;font-weight: bold;color: #008000;");
                    Session.Abandon();
                }
                else
                {
                    Label1.Text = "Sorry,try again later: " + qright + " out of " + temp;
                    Label1.Attributes.Add("style","display:block;margin-top:20px;margin-bottom:20px;border: thick ridge #F20000;font-weight: bold;color: #CC3300;");
                    Session.Abandon();
                }
            }
    }
}
