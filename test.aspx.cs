﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Drawing;

public partial class test : System.Web.UI.Page
{
    List<string> qAtt = new List<string>();
    List<int> questionNo = new List<int>();
    nTest nonTest = new nTest();
    int qno = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (! IsPostBack)
        {
            if (Session.Count > 0)
            {
                if (Session["nName"] != null)
                {
                    Label1.Text += Session["nName"].ToString();
                }
                qRand ql = new qRand();
                Session["qList"] = ql.FiftyQ();
                Session["qCount"] = "0";
                Session["qResult"] = "0";
            }
            else
            {
                Server.Transfer("name.aspx");
            }
            questionNo = (List<int>)Session["qList"];
            qAtt = nonTest.ask( questionNo[Convert.ToInt32(Session["qCount"])]);
            qno = Convert.ToInt32(Session["qCount"]);
            qno ++;
            Label2.Text = "Question: " +  qno;
            Image1.ImageUrl = qAtt[0];
            RadioButtonList1.Items[0].Text = qAtt[1];
            RadioButtonList1.Items[1].Text = qAtt[2];
            RadioButtonList1.Items[2].Text = qAtt[3];
            RadioButtonList1.Items[3].Text = qAtt[4];
            Session["qResult"] = nonTest.rResult();
            if (Request.QueryString["learn"].ToString().Equals("true"))
            {
                showTip();
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        int qc;
        int.TryParse(Session["qCount"].ToString(), out qc);
        Session["qCount"] = qc;
        questionNo = (List<int>)Session["qList"];
        int qID = questionNo[Convert.ToInt32(Session["qCount"])];

                int res = 0;
                int.TryParse(Session["qResult"].ToString(), out res);
                --res;
                RadioButtonList1.Items[res].Attributes.Add("style", "background-color:#99FF99");
                if (RadioButtonList1.SelectedIndex == res)
                {

                    Label3.Text = "Correct";
                    nonTest.processResult(qID, Session["nName"].ToString(), true);
                }
                else
                {

                    Label3.Text = "Wrong";
                    nonTest.processResult(qID, Session["nName"].ToString(), false);
                }

                Timer1.Enabled = true;

        }

    protected void Timer1_Tick1(object sender, EventArgs e)
    {

        RadioButtonList1.SelectedIndex = -1;
        questionNo = (List<int>)Session["qList"];

        int qc;
        int.TryParse(Session["qCount"].ToString(), out qc);
        ++qc;
        Session["qCount"] = qc;
        if(qc > 5){
            Response.Redirect("Result.aspx");
        }
        qAtt = nonTest.ask(questionNo[qc]);
        qno = qc;
        qno++;
        Label2.Text = "Question: " + qno.ToString();
        Image1.ImageUrl = qAtt[0];
        RadioButtonList1.Items[0].Text = qAtt[1];
        RadioButtonList1.Items[1].Text = qAtt[2];
        RadioButtonList1.Items[2].Text = qAtt[3];
        RadioButtonList1.Items[3].Text = qAtt[4];
        Session["qResult"] = nonTest.rResult();

        if (Request.QueryString["learn"].ToString().Equals("true"))
        {
            showTip();
        }

        Timer1.Enabled = false;
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("Result.aspx");
    }
    private void showTip()
    {
        Label4.Text = "Just move your mouse curser over Images for more information";
        Label4.Attributes.Add("style", "font-size:smaller;float:right;");
        int temp = Convert.ToInt32(Session["qCount"]);
        questionNo = (List<int>)Session["qList"];
        ImageTip.Service1 tip = new ImageTip.Service1();
        Image1.ToolTip = tip.getTip(questionNo[temp]);
    }
}
